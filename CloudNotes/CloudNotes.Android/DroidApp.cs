﻿using Android.App;
using Android.Runtime;
using System;

namespace CloudNotes.Droid
{
	[Application]
	[MetaData("com.google.android.maps.v2.API_KEY", Value = CloudNotes.Helpers.Constants.MapApiKey)]
	public class DroidApp : Application
	{
		protected DroidApp(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
		{
		}
	}
}
