﻿using Matcha.BackgroundService.UWP;

namespace CloudNotes.UWP
{
	public sealed partial class MainPage
	{
		public MainPage()
		{
			this.InitializeComponent();

			BackgroundAggregator.Init(this);

			LoadApplication(new CloudNotes.App());
		}
	}
}
