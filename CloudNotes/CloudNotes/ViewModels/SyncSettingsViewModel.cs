﻿using CloudNotes.Helpers;
using CloudNotes.Messages;
using CloudNotes.Services;
using ReactiveUI;
using System;
using System.Reactive;
using System.Reactive.Linq;
using Unity;
using Xamarin.Essentials;

namespace CloudNotes.ViewModels
{
    public class SyncSettingsViewModel : ReactiveObject
    {
        private readonly INoteService noteService;
        private readonly ISettings settings;
        private readonly ObservableAsPropertyHelper<bool> isBusy;
        private string serverAddress;
        private string userName;
        private string password;

        public string ServerAddress
        {
            get => serverAddress;
            set => this.RaiseAndSetIfChanged(ref serverAddress, value);
        }

        public string UserName
        {
            get => userName;
            set => this.RaiseAndSetIfChanged(ref userName, value);
        }

        public string Password
        {
            get => password;
            set => this.RaiseAndSetIfChanged(ref password, value);
        }

        public bool IsBusy => isBusy.Value;

        public ReactiveCommand<Unit, Unit> Sync { get; }

        public SyncSettingsViewModel(INoteService noteService, ISettings settings)
        {
            this.noteService = noteService;

            var syncCanExecute = this.WhenAnyValue(
                x => x.ServerAddress,
                x => x.UserName,
                x => x.Password,
                (server, name, pass) => !(string.IsNullOrEmpty(server) || string.IsNullOrEmpty(name) || string.IsNullOrEmpty(pass)));
            Sync = ReactiveCommand.CreateFromObservable(SyncExecute, syncCanExecute);
            isBusy = Sync.IsExecuting.ToProperty(this, x => x.IsBusy);

            ReadSyncSettings();
            this.settings = settings;
        }

        private IObservable<Unit> SyncExecute()
        {
            return Observable.StartAsync(async () =>
            {
                await settings.SetServerAddress(ServerAddress).ConfigureAwait(false);
                await settings.SetUserName(UserName).ConfigureAwait(false);
                await settings.SetPassword(Password).ConfigureAwait(false);

                var factory = (App.Current as App)?.Ioc.Resolve<IWebDavClientFactory>();
                var result = await noteService.SyncNotes(factory).ConfigureAwait(false);
                if (!result.Succeed)
                    Xamarin.Forms.Device.BeginInvokeOnMainThread(async () => await NavigationService.ShowError(result.ErrorMessage).ConfigureAwait(false));
                else
                    MessageBus.Current.SendMessage(new UpdateMessage(), Constants.UpdateMsg);
            });
        }

        private async void ReadSyncSettings()
        {
            try
            {
                ServerAddress = await SecureStorage.GetAsync("ServerAddress").ConfigureAwait(false);
                UserName = await SecureStorage.GetAsync("UserName").ConfigureAwait(false);
                Password = await SecureStorage.GetAsync("Password").ConfigureAwait(false);
            }
            catch (Exception)
            {
                ServerAddress = Preferences.Get("ServerAddress", null);
                UserName = Preferences.Get("UserName", null);
                Password = Preferences.Get("Password", null);
            }
        }
    }
}
