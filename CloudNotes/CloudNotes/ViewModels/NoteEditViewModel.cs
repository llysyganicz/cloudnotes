﻿using CloudNotes.Helpers;
using CloudNotes.Messages;
using CloudNotes.Models;
using CloudNotes.Services;
using ReactiveUI;
using System;
using System.Collections.ObjectModel;
using System.Reactive;
using System.Reactive.Linq;
using Xamarin.Forms.GoogleMaps;

namespace CloudNotes.ViewModels
{
    public class NoteEditViewModel : ReactiveObject
    {
        private readonly INoteService noteService;
        private bool isNew;
        private bool dateReminder;
        private bool locationReminder;
        private Note note;

        public Note Note
        {
            get => note;
            set => this.RaiseAndSetIfChanged(ref note, value);
        }

        public bool DateReminder
        {
            get => dateReminder;
            set
            {
                dateReminder = value;
                if (Note != null) Note.ReminderEnabled = Note.ReminderEnabled || dateReminder;
                this.RaisePropertyChanged();
            }
        }

        public bool LocationReminder
        {
            get => locationReminder;
            set
            {
                locationReminder = value;
                if (Note != null) Note.ReminderEnabled = Note.ReminderEnabled || locationReminder;
                if (locationReminder) MessageBus.Current.SendMessage((Note?.Latitude, Note?.Longitude), Constants.SetPositionMsg);
                this.RaisePropertyChanged();
            }
        }

        public ObservableCollection<Position> Locations { get; set; }

        public ReactiveCommand<Unit, Unit> Save { get; }

        public ReactiveCommand<Unit, Unit> ClearDateReminder { get; }

        public ReactiveCommand<Unit, Unit> ClearLocationReminder { get; }

        public ReactiveCommand<MapLongClickedEventArgs, Unit> SetLocation { get; }

        public NoteEditViewModel(INoteService noteService)
        {
            this.noteService = noteService;
            MessageBus.Current.Listen<string>(Constants.EditMsg).Subscribe(InitNote);

            Save = ReactiveCommand.CreateFromObservable(SaveExecute);

            var canCleanDateReminder = this.WhenAnyValue(x => x.Note, note => note?.DateTimeReminder.HasValue ?? false);
            ClearDateReminder = ReactiveCommand.Create(ClearDateReminderExecute, canCleanDateReminder);

            var canClearLocationReminder = this.WhenAnyValue(x => x.Note, note => note?.Longitude.HasValue ?? false);
            ClearLocationReminder = ReactiveCommand.Create(ClearLocationReminderExecute, canClearLocationReminder);

            SetLocation = ReactiveCommand.Create<MapLongClickedEventArgs, Unit>(e =>
            {
                SetLocationExecute(e);
                return Unit.Default;
            });
        }

        private void InitNote(string id)
        {
            isNew = string.IsNullOrEmpty(id);

            Locations = new ObservableCollection<Position>();

            if (isNew)
            {
                Note = new Note
                {
                    Id = Guid.NewGuid().ToString(),
                    DateTimeReminder = null,
                    Latitude = null,
                    Longitude = null,
                };
            }
            else
            {
                this.noteService.GetNote(id).Match(
                    note =>
                    {
                        Note = note;
                        DateReminder = Note.ReminderEnabled && Note.DateTimeReminder.HasValue;
                        LocationReminder = Note.ReminderEnabled && Note.Longitude.HasValue;
                        if (Note.Longitude.HasValue) Locations.Add(new Position(Note.Latitude.Value, Note.Longitude.Value));
                    },
                    async () =>
                    {
                        await NavigationService.ShowError("Can't find note.").ConfigureAwait(false);
                        await NavigationService.GoBack().ConfigureAwait(false);
                    });
            }
        }

        private IObservable<Unit> SaveExecute()
        {
            return Observable.StartAsync(async () =>
            {
                var result = isNew ? noteService.AddNote(Note) :
                                     noteService.UpdateNote(Note);

                if (!result.Succeed)
                {
                    await NavigationService.ShowError(result.ErrorMessage).ConfigureAwait(false);
                }
                else
                {
                    MessageBus.Current.SendMessage(new UpdateMessage(), Constants.UpdateMsg);
                    await NavigationService.GoBack().ConfigureAwait(false);
                }
            });
        }

        private void ClearDateReminderExecute()
        {
            Note.DateTimeReminder = null;
            DateReminder = false;
        }

        private void ClearLocationReminderExecute()
        {
            Note.Latitude = Note.Longitude = null;
            Locations.Clear();
            LocationReminder = false;
        }

        private void SetLocationExecute(MapLongClickedEventArgs parameter)
        {
            if (Locations.Count >= 1) Locations.Clear();
            Locations.Add(parameter.Point);
            Note.Latitude = parameter.Point.Latitude;
            Note.Longitude = parameter.Point.Longitude;
        }
    }
}
