﻿
using Unity;

namespace CloudNotes.ViewModels
{
	public class ViewModelLocator
	{
		private UnityContainer ioc;

		public ViewModelLocator()
		{
			ioc = (App.Current as App).Ioc;
		}

		public NoteListViewModel ListViewModel => ioc.Resolve<NoteListViewModel>();
		public NoteEditViewModel EditViewModel => ioc.Resolve<NoteEditViewModel>();
		public SyncSettingsViewModel SyncSettingsViewModel => ioc.Resolve<SyncSettingsViewModel>();
	}
}
