﻿using CloudNotes.Helpers;
using CloudNotes.Messages;
using CloudNotes.Models;
using CloudNotes.Services;
using ReactiveUI;
using System;
using System.Collections.ObjectModel;
using System.Reactive;
using System.Reactive.Linq;

namespace CloudNotes.ViewModels
{
    public class NoteListViewModel : ReactiveObject
    {
        private readonly INoteService noteService;
        private ObservableCollection<Note> notes;
        private readonly ObservableAsPropertyHelper<bool> isRefreshing;

        public ObservableCollection<Note> Notes
        {
            get => notes;
            set => this.RaiseAndSetIfChanged(ref notes, value);
        }

        public bool IsRefreshing => isRefreshing.Value;

        public ReactiveCommand<Unit, Unit> UpdateList { get; }

        public ReactiveCommand<Unit, Unit> NewNote { get; }

        public ReactiveCommand<string, Unit> EditNote { get; }

        public ReactiveCommand<string, Unit> RemoveNote { get; }

        public NoteListViewModel(INoteService noteService)
        {
            this.noteService = noteService;
            Notes = new ObservableCollection<Note>();

            UpdateList = ReactiveCommand.CreateFromObservable(UpdateNotes);

            NewNote = ReactiveCommand.Create(NewNoteExecute);

            EditNote = ReactiveCommand.CreateFromObservable<string, Unit>(EditNoteExecute);

            RemoveNote = ReactiveCommand.CreateFromObservable<string, Unit>(RemoveNoteExecute);
            RemoveNote.IsExecuting.Skip(1).Where(x => !x).Subscribe(_ => UpdateList.Execute());

            isRefreshing = UpdateList.IsExecuting.ToProperty(this, x => x.IsRefreshing);

            UpdateList.Execute();
            MessageBus.Current.Listen<UpdateMessage>(Constants.UpdateMsg).Subscribe(_ => UpdateList.Execute());
        }

        private IObservable<Unit> UpdateNotes()
        {
            return Observable.Start(() =>
            {
                Notes.Clear();
                foreach (var note in noteService.GetAllNotes())
                    Notes.Add(note);
            });
        }

        private async void NewNoteExecute()
        {
            await NavigationService.NavigateTo(Constants.EditPageKey).ConfigureAwait(false);
            MessageBus.Current.SendMessage(string.Empty, Constants.EditMsg);
        }

        private IObservable<Unit> EditNoteExecute(string id)
        {
            return Observable.StartAsync(async () =>
            {
                await NavigationService.NavigateTo(Constants.EditPageKey).ConfigureAwait(false);
                MessageBus.Current.SendMessage(id, Constants.EditMsg);
            });
        }

        private IObservable<Unit> RemoveNoteExecute(string id)
        {
            return Observable.StartAsync(async () =>
            {
                var result = noteService.RemoveNote(id);
                if (!result.Succeed)
                    await NavigationService.ShowError(result.ErrorMessage).ConfigureAwait(false);
            });
        }
    }
}
