﻿using System;
using System.Net;
using WebDav;

namespace CloudNotes.Services
{
	public class WebDavClientFactory : IWebDavClientFactory
	{
		public IWebDavClient CreateWebDavClient(string userName, string password, string serverAddress)
		{
			var parameters = new WebDavClientParams
			{
				BaseAddress = new Uri(serverAddress),
				Credentials = new NetworkCredential(userName, password)
			};

			return new WebDavClient(parameters);
		}
	}
}
