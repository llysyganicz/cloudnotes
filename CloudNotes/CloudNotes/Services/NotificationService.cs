﻿using CloudNotes.Helpers;
using Matcha.BackgroundService;
using Plugin.LocalNotifications;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CloudNotes.Services
{
	public class NotificationService : IPeriodicTask
	{
		private readonly INoteService noteService;

		public NotificationService(INoteService noteService)
		{
			this.noteService = noteService;
			Interval = TimeSpan.FromMinutes(1);
		}

		public TimeSpan Interval { get; }

		public async Task<bool> StartJob()
		{
			var notes = noteService.GetAllNotes().Where(note => note.ReminderEnabled);

			foreach (var note in notes)
			{
				if (note.DateTimeReminder.HasValue && note.DateTimeReminder <= DateTimeOffset.Now)
				{
					CrossLocalNotifications.Current.Show("CloudNotes reminder", note.Title);
					note.ReminderEnabled = false;
					noteService.UpdateNote(note);
				}

				if (note.Longitude.HasValue)
				{
					var location = await Geolocation.GetLocation();
					var distance = Xamarin.Essentials.Location.CalculateDistance(
						location.Latitude, 
						location.Longitude, 
						note.Latitude.Value, 
						note.Longitude.Value, 
						Xamarin.Essentials.DistanceUnits.Kilometers);
					if(distance * 1000 < 300)
					{
						CrossLocalNotifications.Current.Show("CloudNotes reminder", note.Title);
						note.ReminderEnabled = false;
						noteService.UpdateNote(note);
					}
				}
			}

			return true;
		}
	}
}
