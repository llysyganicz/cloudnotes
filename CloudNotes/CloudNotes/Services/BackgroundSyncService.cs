﻿using CloudNotes.Helpers;
using CloudNotes.Messages;
using Matcha.BackgroundService;
using ReactiveUI;
using System;
using System.Threading.Tasks;

namespace CloudNotes.Services
{
    public class BackgroundSyncService : IPeriodicTask
    {
        private readonly INoteService noteService;
        private readonly IWebDavClientFactory webDavClientFactory;

        public BackgroundSyncService(INoteService noteService, IWebDavClientFactory webDavClientFactory)
        {
            this.noteService = noteService;
            this.webDavClientFactory = webDavClientFactory;
            Interval = TimeSpan.FromMinutes(3);
        }

        public TimeSpan Interval { get; }

        public async Task<bool> StartJob()
        {
            var result = await noteService.SyncNotes(webDavClientFactory);
            if (result.Succeed == true) MessageBus.Current.SendMessage(new UpdateMessage(), Constants.UpdateMsg);

            return true;
        }
    }
}
