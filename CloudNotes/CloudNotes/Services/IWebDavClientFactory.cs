﻿using System;
using System.Net;
using WebDav;

namespace CloudNotes.Services
{

	public interface IWebDavClientFactory
	{
		IWebDavClient CreateWebDavClient(string userName, string password, string serverAddress);
	}
}
