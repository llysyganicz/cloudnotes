﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CloudNotes.Helpers;
using CloudNotes.Models;
using LiteDB;
using Newtonsoft.Json;
using Optional;
using Unity;
using WebDav;
using Xamarin.Essentials;

namespace CloudNotes.Services
{
	public class NoteService : INoteService
	{
		private readonly string connectionString;
		private readonly ISettings settings;
		private readonly IConnectivity connectivity;

		public NoteService(string connectionString, ISettings settings, IConnectivity connectivity)
		{
			this.connectionString = connectionString;
			this.settings = settings;
			this.connectivity = connectivity;
		}

		public OperationResult AddNote(Note noteToAdd)
		{
			try
			{
				using (var db = new LiteRepository(connectionString))
				{
					noteToAdd.Id = Guid.NewGuid().ToString();
					noteToAdd.LastChanged = DateTimeOffset.Now;
					var id = db.Insert<Note>(noteToAdd);

					return OperationResult.Success();
				}
			}
			catch (Exception ex)
			{
				return OperationResult.Fail(ex.Message);
			}
		}

		public List<Note> GetAllNotes()
		{
			using (var db = new LiteRepository(connectionString))
			{
				return db.Query<Note>().Where(note => note.IsDeleted == false).ToList();
			}
		}

		public Option<Note> GetNote(string id)
		{
			using (var db = new LiteRepository(connectionString))
			{
				var result = db.Query<Note>().Where(note => note.IsDeleted == false)
											 .Where(note => note.Id == id).FirstOrDefault();
				return result.SomeNotNull();
			}
		}

		public OperationResult RemoveNote(string id)
		{
			try
			{
				using (var db = new LiteRepository(connectionString))
				{
					var note = db.Query<Note>().Where(n => n.IsDeleted == false)
											 .Where(n => n.Id == id).FirstOrDefault();
					if (note != null)
					{
						note.IsDeleted = true;
						db.Update<Note>(note);
						return OperationResult.Success();
					}

					return OperationResult.Fail($"Can't find note.");
				}
			}
			catch (Exception ex)
			{
				return OperationResult.Fail(ex.Message);
			}
		}

		public OperationResult UpdateNote(Note noteToUpdate)
		{
			try
			{
				using (var db = new LiteRepository(connectionString))
				{
					var note = db.Query<Note>().Where(n => n.IsDeleted == false)
											 .Where(n => n.Id == noteToUpdate.Id).FirstOrDefault();
					if (note != null)
					{
						note.Title = noteToUpdate.Title;
						note.Content = noteToUpdate.Content;
						note.ReminderEnabled = noteToUpdate.ReminderEnabled;
						note.DateTimeReminder = noteToUpdate.DateTimeReminder;
						note.Longitude = noteToUpdate.Longitude;
						note.Latitude = noteToUpdate.Latitude;
						note.IsChanged = true;
						note.LastChanged = DateTimeOffset.Now;
						db.Update<Note>(note);
						return OperationResult.Success();
					}

					return OperationResult.Fail($"Can't find note.");
				}
			}
			catch (Exception ex)
			{
				return OperationResult.Fail(ex.Message);
			}
		}

		public async Task<OperationResult> SyncNotes(IWebDavClientFactory webDavClientFactory)
		{
			var userName = await settings.GetUserName();
			var password = await settings.GetPassword();
			var server = await settings.GetServerAddress();

			// sync not configured or no internet connection - return success -> no error
			if (userName == null || password == null || server == null || connectivity.HaveInternetConnection == false)
				return OperationResult.Success();

			using (var webDavClient = webDavClientFactory.CreateWebDavClient(userName, password, server))
				return await Sync(webDavClient);
		}

		private async Task<bool> EnsureRootFolderExists(IWebDavClient client)
		{
			var list = await client.Propfind("");
			if (list.IsSuccessful)
			{
				var folder = list.Resources.FirstOrDefault(x => x.Uri.EndsWith("CloudNotes/"));
				if (folder != null) return true;

				var response = await client.Mkcol("CloudNotes");
				return response.IsSuccessful;
			}

			return false;
		}

		private async Task<Note> DownloadNote(IWebDavClient client, string file)
		{
			using (var tempFile = File.Open(Path.GetTempFileName(), System.IO.FileMode.Open))
			{
				using (var response = await client.GetRawFile(file))
					await response.Stream.CopyToAsync(tempFile);

				var buffer = new byte[tempFile.Length];
				tempFile.Seek(0, SeekOrigin.Begin);
				tempFile.Read(buffer, 0, buffer.Length);

				var json = System.Text.Encoding.UTF8.GetString(buffer);

				var note = JsonConvert.DeserializeObject<Note>(json);

				return note;
			}
		}

		private async Task<bool> UploadNote(IWebDavClient client, Note note)
		{
			var json = JsonConvert.SerializeObject(note);
			var buffer = System.Text.Encoding.UTF8.GetBytes(json);

			using (var stream = new MemoryStream(buffer))
			{
				var result = await client.PutFile($"CloudNotes/{note.Id}", stream);
				return result.IsSuccessful;
			}
		}

		private async Task<OperationResult> Sync(IWebDavClient client)
		{
			var syncFailed = false;

			if (await EnsureRootFolderExists(client) == false) return OperationResult.Fail("Can't access CloudNotes folder.");

			var files = (await client.Propfind("CloudNotes/")).Resources.Where(x => x.IsCollection == false).ToList();

			using (var db = new LiteRepository(connectionString))
			{
				var notes = db.Query<Note>().ToList();

				foreach (var note in notes)
				{
					var file = files.FirstOrDefault(f => f.Uri.EndsWith(note.Id));

					if (note.IsDeleted)
					{
						if (file != null)
						{
							var result = await client.Delete(file.Uri);
							if (result.IsSuccessful) db.Delete<Note>(note.Id);
							files.Remove(file);
							syncFailed = !result.IsSuccessful || syncFailed;
						}
						else db.Delete<Note>(note.Id);
					}
					else if (note.IsChanged && file != null)
					{
						var serverNote = await DownloadNote(client, file.Uri);
						if (serverNote.LastChanged > note.LastChanged) db.Update<Note>(serverNote);
						else
						{
							note.IsChanged = false;
							var result = await UploadNote(client, note);
							if (result) db.Update<Note>(note);
							syncFailed = !result || syncFailed;
						}
						files.Remove(file);
					}
					else if (file == null)
					{
						note.IsChanged = false;
						var result = await UploadNote(client, note);
						if (result) db.Update<Note>(note);
						syncFailed = !result || syncFailed;
					}
					else files.Remove(file);
				}

				foreach (var serverFile in files)
				{
					var note = await DownloadNote(client, serverFile.Uri);
					var result = db.Insert<Note>(note) != null;
					syncFailed = !result || syncFailed;
				}
			}

			return syncFailed ? OperationResult.Fail("Synchronization failed.") : OperationResult.Success();

		}
	}
}
