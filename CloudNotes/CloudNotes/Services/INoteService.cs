﻿using CloudNotes.Models;
using Optional;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CloudNotes.Services
{
	public interface INoteService
	{
		List<Note> GetAllNotes();
		Option<Note> GetNote(string id);
		OperationResult AddNote(Note noteToAdd);
		OperationResult UpdateNote(Note noteToUpdate);
		OperationResult RemoveNote(string id);
		Task<OperationResult> SyncNotes(IWebDavClientFactory webDavClientFactory);
	}
}
