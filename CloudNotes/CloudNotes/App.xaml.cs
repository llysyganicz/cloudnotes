﻿using CloudNotes.Helpers;
using CloudNotes.Services;
using CloudNotes.ViewModels;
using CloudNotes.Views;
using Matcha.BackgroundService;
using System.IO;
using Unity;
using Unity.Injection;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace CloudNotes
{
	public partial class App : Application
	{
		private string connectionString = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData), "notes.db");
		public UnityContainer Ioc = new UnityContainer();

		public App()
		{
			RegisterDependencies();
			InitializeComponent();

			MainPage = Ioc.Resolve<MainPage>();
		}

		protected override void OnStart()
		{
			BackgroundAggregatorService.StopBackgroundService();
		
			BackgroundAggregatorService.Add(() => Ioc.Resolve<NotificationService>());
			BackgroundAggregatorService.Add(() => Ioc.Resolve<BackgroundSyncService>());

			BackgroundAggregatorService.StartBackgroundService();
		}

		protected override void OnSleep()
		{
		}

		protected override void OnResume()
		{
		}

		private void RegisterDependencies()
		{
			Ioc.AddExtension(new Diagnostic());

			Ioc.RegisterType<IWebDavClientFactory, WebDavClientFactory>();
			Ioc.RegisterType<ISettings, Settings>();
			Ioc.RegisterType<IConnectivity, Connectivity>();
			Ioc.RegisterType<INoteService, NoteService>(
				new InjectionConstructor(connectionString, Ioc.Resolve<ISettings>(), Ioc.Resolve<IConnectivity>()));
			Ioc.RegisterType<BackgroundSyncService>();
			Ioc.RegisterType<NotificationService>();

			Ioc.RegisterType<NoteListViewModel>();
			Ioc.RegisterType<NoteEditViewModel>();
			Ioc.RegisterType<SyncSettingsViewModel>();

			Ioc.RegisterType<SyncSettingsPage>();
			Ioc.RegisterType<ListPage>();
			Ioc.RegisterType<EditPage>();
			Ioc.RegisterType<MainPage>();

			NavigationService.Register(Constants.EditPageKey, typeof(EditPage));
			NavigationService.Register(Constants.ListPageKey, typeof(ListPage));
			NavigationService.Register(Constants.SyncSettingsKey, typeof(SyncSettingsPage));
		}
	}
}
