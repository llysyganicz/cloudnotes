﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity;
using Xamarin.Forms;

namespace CloudNotes.Helpers
{
	public static class NavigationService
	{
		private static Dictionary<string, Type> mappings = new Dictionary<string, Type>();

		private static NavigationPage nav => (Application.Current.MainPage as TabbedPage).Children[0] as NavigationPage;

		public static void Register(string key, Type pageType) => mappings[key] = pageType;

		public static Page CurrentPage => nav.CurrentPage;

		public static async Task NavigateTo(string key)
		{
			var page = (App.Current as App).Ioc.Resolve(mappings[key]) as Page;
			
			if (page != null) await nav.PushAsync(page, true);
		}

		public static async Task GoBack() => await nav.PopAsync(true);

		public static async Task GoToRoot() => await nav.PopToRootAsync(true);

		public static async Task ShowError(string message)
		{
			await Application.Current.MainPage.DisplayAlert("Error", message, "Ok");
		}
	}
}
