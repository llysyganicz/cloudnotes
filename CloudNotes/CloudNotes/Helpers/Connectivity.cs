﻿using Xamarin.Essentials;

namespace CloudNotes.Helpers
{
	public class Connectivity : IConnectivity
	{
		public bool HaveInternetConnection => Xamarin.Essentials.Connectivity.NetworkAccess == NetworkAccess.Internet;
	}
}
