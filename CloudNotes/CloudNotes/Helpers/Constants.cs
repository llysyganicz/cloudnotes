﻿namespace CloudNotes.Helpers
{
	public static class Constants
	{
		public const string SyncSettingsKey = "SyncSettingsPage";
		public const string ListPageKey = "ListPage";
		public const string EditPageKey = "EditPage";

		public const string EditMsg = "Edit";
		public const string UpdateMsg = "Update";
		public const string SetPositionMsg = "SetPosition";

		public const string MapApiKey = Secrets.MapApiKey;
	}
}
