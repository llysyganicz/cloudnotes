﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CloudNotes.Helpers
{
	public class EventToCommand : Behavior<View>
	{
		private Delegate eventHandler;

		protected override void OnAttachedTo(View bindable)
		{
			base.OnAttachedTo(bindable);
			RegisterEvent(bindable);
		}

		protected override void OnDetachingFrom(View bindable)
		{
			UnregisterEvente(bindable);
			base.OnDetachingFrom(bindable);
		}

		private void RegisterEvent(View view)
		{
			if (string.IsNullOrWhiteSpace(EventName)) return;

			var eventInfo = view.GetType().GetRuntimeEvent(EventName);
			if (eventInfo == null) throw new ArgumentException(string.Format("EventToCommandBehavior: Can't register the '{0}' event.", EventName));

			var methodInfo = typeof(EventToCommand).GetTypeInfo().GetDeclaredMethod("OnEvent");
			eventHandler = methodInfo.CreateDelegate(eventInfo.EventHandlerType, this);
			eventInfo.AddEventHandler(view, eventHandler);
		}

		private void UnregisterEvente(View view)
		{
			if (string.IsNullOrWhiteSpace(EventName) || eventHandler == null) return;

			var eventInfo = view.GetType().GetRuntimeEvent(EventName);
			if (eventInfo == null) throw new ArgumentException(string.Format("EventToCommandBehavior: Can't register the '{0}' event.", EventName));

			eventInfo.RemoveEventHandler(view, eventHandler);
			eventHandler = null;
		}

		private void OnEvent(object sender, EventArgs e)
		{
			if (Command == null) return;

			object parameter;
			if (CommandParameter != null) parameter = CommandParameter;
			else if (Converter != null) parameter = Converter.Convert(e, typeof(object), null, null);
			else parameter = e;

			if (Command.CanExecute(parameter)) Command.Execute(parameter);

		}

		public static readonly BindableProperty EventNameProperty = BindableProperty.Create(
			"EventName", typeof(string), typeof(EventToCommand), null);

		public static readonly BindableProperty CommandProperty = BindableProperty.Create(
			"Command", typeof(ICommand), typeof(EventToCommand), null);

		public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create(
			"CommandParameter", typeof(object), typeof(EventToCommand), null);

		public static readonly BindableProperty ConverterProperty = BindableProperty.Create(
			"Converter", typeof(IValueConverter), typeof(EventToCommand), null);

		public string EventName
		{
			get => (string)GetValue(EventNameProperty);
			set => SetValue(EventNameProperty, value);
		}

		public ICommand Command
		{
			get => (ICommand)GetValue(CommandProperty);
			set => SetValue(CommandProperty, value);
		}

		public object CommandParameter
		{
			get => GetValue(CommandParameterProperty);
			set => SetValue(CommandParameterProperty, value);
		}

		public IValueConverter Converter
		{
			get => (IValueConverter)GetValue(ConverterProperty);
			set => SetValue(ConverterProperty, value);
		}
	}
}
