﻿using System;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms.GoogleMaps;

namespace CloudNotes.Helpers
{
	public static class Geolocation
	{
		public static async Task<Position> GetLocation()
		{
			try
			{
				var result = await Xamarin.Essentials.Geolocation.GetLocationAsync();
				return new Position(result.Latitude, result.Longitude);
			}
			catch (FeatureNotSupportedException fnsEx)
			{
				throw;
			}
			catch (FeatureNotEnabledException fneEx)
			{
				throw;
			}
			catch (PermissionException pEx)
			{
				throw;
			}
			catch (Exception ex)
			{
				throw;
			}
		}
	}
}
