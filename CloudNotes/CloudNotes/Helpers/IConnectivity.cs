﻿namespace CloudNotes.Helpers
{
	public interface IConnectivity
	{
		bool HaveInternetConnection { get; }

	}
}
