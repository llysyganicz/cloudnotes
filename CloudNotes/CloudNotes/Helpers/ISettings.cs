﻿using System.Threading.Tasks;

namespace CloudNotes.Helpers
{
	public interface ISettings
	{
		Task<string> GetServerAddress();
		Task<string> GetUserName();
		Task<string> GetPassword();
		Task SetServerAddress(string address);
		Task SetUserName(string userName);
		Task SetPassword(string password);
	}
}
