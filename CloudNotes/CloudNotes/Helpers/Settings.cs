﻿using System;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace CloudNotes.Helpers
{
	public class Settings : ISettings
	{
		public async Task<string> GetPassword()
		{
			try
			{
				return await SecureStorage.GetAsync("Password");
			}
			catch (Exception)
			{
				return Preferences.Get("Password", null);
			}
		}

		public async Task<string> GetServerAddress()
		{
			try
			{
				return await SecureStorage.GetAsync("ServerAddress");
			}
			catch (Exception)
			{
				return Preferences.Get("ServerAddress", null);
			}
		}

		public async Task<string> GetUserName()
		{
			try
			{
				return await SecureStorage.GetAsync("UserName");
			}
			catch (Exception)
			{
				return Preferences.Get("UserName", null);
			}
		}

		public async Task SetPassword(string password)
		{
			try
			{
				await SecureStorage.SetAsync("Password", password);
			}
			catch (Exception)
			{
				Preferences.Set("Password", password);
			}
		}

		public async Task SetServerAddress(string address)
		{
			try
			{
				await SecureStorage.SetAsync("ServerAddress", address);
			}
			catch (Exception)
			{
				Preferences.Set("ServerAddress", address);
			}
		}

		public async Task SetUserName(string userName)
		{
			try
			{
				await SecureStorage.SetAsync("UserName", userName);
			}
			catch (Exception)
			{
				Preferences.Set("UserName", userName);
			}
		}
	}
}
