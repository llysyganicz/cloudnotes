﻿using CloudNotes.Helpers;
using ReactiveUI;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace CloudNotes.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditPage : ContentPage
    {
        public EditPage()
        {
            InitializeComponent();

            MessageBus.Current.Listen<(double?, double?)>(Constants.SetPositionMsg).Subscribe(async x => await SetMapLocation(x));
        }

        private async Task SetMapLocation((double?, double?) position)
        {
            Position location;
            if (position.Item1 != null) location = new Position(position.Item1.Value, position.Item2.Value);
            else location = await Geolocation.GetLocation();

            Device.BeginInvokeOnMainThread(() => map.MoveToRegion(MapSpan.FromCenterAndRadius(location, new Distance(1000.0))));
        }
    }
}