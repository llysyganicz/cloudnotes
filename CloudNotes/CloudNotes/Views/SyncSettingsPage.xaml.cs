﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CloudNotes.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SyncSettingsPage : ContentPage
	{
		public SyncSettingsPage()
		{
			InitializeComponent ();
		}
	}
}