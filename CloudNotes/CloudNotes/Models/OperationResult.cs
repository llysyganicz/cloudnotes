﻿namespace CloudNotes.Models
{
	public class OperationResult<T>
	{
		public T Result { get; private set; }
		public bool Succeed { get; private set; }
		public string ErrorMessage { get; private set; }

		public static OperationResult<T> Success(T value) => new OperationResult<T>
		{
			Result = value,
			Succeed = true
		};

		public static OperationResult<T> Fail(string errorMessage) => new OperationResult<T> { ErrorMessage = errorMessage };
	}

	public class OperationResult
	{
		public bool Succeed { get; private set; }
		public string ErrorMessage { get; private set; }

		public static OperationResult Success() => new OperationResult { Succeed = true };

		public static OperationResult Fail(string errorMessage) => new OperationResult { ErrorMessage = errorMessage };
	}
}
