﻿using LiteDB;
using System;

namespace CloudNotes.Models
{
	public class Note
	{
		[BsonId]
		public string Id { get; set; }
		public string Title { get; set; }
		public string Content { get; set; }
		public bool IsDeleted { get; set; }
		public bool IsChanged { get; set; }
		public DateTimeOffset LastChanged { get; set; }
		public bool ReminderEnabled { get; set; }
		public DateTimeOffset? DateTimeReminder { get; set; }
		public double? Longitude { get; set; }
		public double? Latitude { get; set; }
	}
}
