module ``NoteService Tests``

open System
open CloudNotes.Models
open CloudNotes.Services
open FsUnit
open Xunit
open CloudNotes.Tests
open LiteDB
open WebDav
open Foq
open System.Threading.Tasks
open System.IO
open System.Net.Http
open Newtonsoft.Json
open CloudNotes.Helpers

let noteId = "0228A11C-2A09-41C1-8281-733CF0715AE6"
let deletedNoteId = "403114DE-CA78-4C9E-ABF5-3C44D7B23997"
let serverNoteId = "60705508-4A91-4368-AF52-A4018030CE0E"
let lastChangedDate = new DateTimeOffset(new DateTime(2019, 1, 1, 12, 0, 0))
let connectivity = Mock<IConnectivity>().Setup(fun c -> <@ c.HaveInternetConnection @>).Returns(true).Create()

let prepareDatabase (db: LiteRepository) =
    let note = new Note(Id = noteId, Title = "Note", Content = "Note", LastChanged = lastChangedDate)
    let deletedNote = new Note(Id = deletedNoteId, Title = "Deleted note", Content = "Deleted note", IsDeleted = true, LastChanged = lastChangedDate)
    db.Insert(note) |> ignore
    db.Insert(deletedNote) |> ignore

[<Fact>]
let ``AddNote returns OperationResult with Succeed equals true when note is added`` () =
    use file = new TempFile()
    use db = new LiteRepository(file.FileName)
    prepareDatabase db
    let settings = Mock<ISettings>().Create()
    let service = new NoteService(file.FileName, settings, connectivity)

    let result = service.AddNote(Note(Title = "New note", Content = "New note"))

    result |> should be ofExactType<OperationResult>
    result.Succeed |> should be True
    db.Query<Note>().Count() |> should equal 3

[<Fact>]
let ``AddNote returns OperationResult with Succeed equals false when added failed`` () =
    let settings = Mock<ISettings>().Create()
    let service = new NoteService(null, settings, connectivity)

    let result = service.AddNote(Note(Title = "Title", Content = "Content"))

    result |> should be ofExactType<OperationResult>
    result.Succeed |> should be False
    result.ErrorMessage |> should not' (be null) 

[<Fact>]
let ``GetAllNotes returns 1 note`` () =
    use file = new TempFile()
    use db = new LiteRepository(file.FileName)
    prepareDatabase db
    let settings = Mock<ISettings>().Create()
    let service = new NoteService(file.FileName, settings, connectivity)

    let result = service.GetAllNotes()
    
    result |> should haveCount 1
    result.[0] |> should be ofExactType<Note>
    result.[0].Id |> should equal noteId

[<Fact>]
let ``GetNote returns valid result`` () =
    use file = new TempFile()
    use db = new LiteRepository(file.FileName)
    prepareDatabase db
    let settings = Mock<ISettings>().Create()
    let service = new NoteService(file.FileName, settings, connectivity)

    let noteResult = service.GetNote(noteId)
    let deletedNoteResult = service.GetNote(deletedNoteId)

    noteResult.HasValue |> should be True
    noteResult.ValueOr(new Note()).Id |> should equal noteId 
    deletedNoteResult.HasValue |> should be False

[<Fact>]
let ``RemoveNote returns OperationResult with Succeed equals true when IsDeleted is set to true`` () =
    use file = new TempFile()
    use db = new LiteRepository(file.FileName)
    prepareDatabase db
    let settings = Mock<ISettings>().Create()
    let service = new NoteService(file.FileName, settings, connectivity)

    let result = service.RemoveNote(noteId)

    result.Succeed |> should be True
    db.Query<Note>().Where(fun n -> n.Id = noteId).Where(fun n -> n.IsDeleted = true).Count() |> should equal 1

[<Fact>]
let ``RemoveNote returns OperationResult with Succeed equals false for invalid note id`` () =
    use file = new TempFile()
    use db = new LiteRepository(file.FileName)
    prepareDatabase db
    let settings = Mock<ISettings>().Create()
    let service = new NoteService(file.FileName, settings, connectivity)

    let result = service.RemoveNote(deletedNoteId)

    result.Succeed |> should be False
    result.ErrorMessage |> should equal "Can't find note."

[<Fact>]
let ``RemoveNote returns OperationResult with Succeed equals false for exception`` () =
    let settings = Mock<ISettings>().Create()
    let service = new NoteService(null, settings, connectivity)

    let result = service.RemoveNote(noteId)

    result.Succeed |> should be False
    result.ErrorMessage |> should not' (be null)

[<Fact>]
let ``UpdateNote returns OperationResult with Succeed equals true when note updated`` () =
    use file = new TempFile()
    use db = new LiteRepository(file.FileName)
    prepareDatabase db
    let settings = Mock<ISettings>().Create()
    let service = new NoteService(file.FileName, settings, connectivity)

    let result = service.UpdateNote(Note(Id = noteId, Title = "Edited title", Content = "Edited content"))
    let updatedNote = db.Query<Note>().Where(fun n -> n.Id = noteId).FirstOrDefault()

    result.Succeed |> should be True
    updatedNote.Content |> should equal "Edited content"
    updatedNote.IsChanged |> should be True
    updatedNote.LastChanged |> should not' (equal lastChangedDate)
    updatedNote.Title |> should equal "Edited title"

[<Fact>]
let ``UpdateNote returns OperationResult with Succeed equals false for invalid note id`` () =
    use file = new TempFile()
    use db = new LiteRepository(file.FileName)
    prepareDatabase db
    let settings = Mock<ISettings>().Create()
    let service = new NoteService(file.FileName, settings, connectivity)

    let result = service.UpdateNote(Note(Id = deletedNoteId, Title = "Edited title", Content = "Edited content"))

    result.Succeed |> should be False
    result.ErrorMessage |> should equal "Can't find note."

[<Fact>]
let ``UpdateNote returns OperationResult with Succeed equals false for exception`` () =
    let settings = Mock<ISettings>().Create()
    let service = new NoteService(null, settings, connectivity)

    let result = service.UpdateNote(Note(Id = noteId, Title = "Edited title", Content = "Edited content"))

    result.Succeed |> should be False
    result.ErrorMessage |> should not' (be null)

[<Fact>]
let ``SyncNotes returns OperationResult with Succeed equals false when root folder can not be found`` () =
    use file = new TempFile()
    use db = new LiteRepository(file.FileName)
    prepareDatabase db
    let settings = Mock<ISettings>().Setup(fun s -> <@ s.GetServerAddress() @>).Returns(Task.FromResult("server"))
                                    .Setup(fun s -> <@ s.GetUserName() @>).Returns(Task.FromResult("user"))
                                    .Setup(fun s -> <@ s.GetPassword() @>).Returns(Task.FromResult("password")).Create()
    let service = new NoteService(file.FileName, settings, connectivity)
    let response = new PropfindResponse(404)
    let webDavClient = Mock<IWebDavClient>().Setup(fun c -> <@ c.Propfind "" @>).Returns(Task.FromResult(response)).Create()
    let webDavClientFactory = Mock<IWebDavClientFactory>()
                               .Setup(fun f -> <@ f.CreateWebDavClient(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()) @>)
                               .Returns(webDavClient).Create()

    let result = service.SyncNotes(webDavClientFactory).Result

    result.Succeed |> should be False
    result.ErrorMessage |> should equal "Can't access CloudNotes folder."

[<Fact>]
let ``SyncNotes returns OperationResult with Succeed equals false when any object is not synced correctly`` () =
    use file = new TempFile()
    use db = new LiteRepository(file.FileName)
    prepareDatabase db
    let settings = Mock<ISettings>().Setup(fun s -> <@ s.GetServerAddress() @>).Returns(Task.FromResult("server"))
                                    .Setup(fun s -> <@ s.GetUserName() @>).Returns(Task.FromResult("user"))
                                    .Setup(fun s -> <@ s.GetPassword() @>).Returns(Task.FromResult("password")).Create()
    let service = new NoteService(file.FileName, settings, connectivity)
    let response = new PropfindResponse(200, new System.Collections.Generic.List<WebDavResource>())
    let resourceBuilder = new WebDavResource.Builder()
    (response.Resources :?> System.Collections.Generic.List<WebDavResource>).Add(resourceBuilder
                                                                                  .IsCollection()
                                                                                  .WithUri("/CloudNotes/").Build())
    let webDavClient = Mock<IWebDavClient>()
                        .Setup(fun c -> <@ c.Propfind(It.IsAny<string>()) @>)
                        .Returns(Task.FromResult(response))
                        .Setup(fun c -> <@ c.PutFile(It.IsAny<string>(), It.IsAny<Stream>()) @>)
                        .Returns(Task.FromResult(new WebDavResponse(400)))
                        .Create()
    let webDavClientFactory = Mock<IWebDavClientFactory>()
                               .Setup(fun f -> <@ f.CreateWebDavClient(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()) @>)
                               .Returns(webDavClient).Create()

    let result = service.SyncNotes(webDavClientFactory).Result

    result.Succeed |> should be False
    result.ErrorMessage |> should equal "Synchronization failed."

[<Fact>]
let ``SyncNotes returns OperationResult with Succeed equals true when all objects are synced correctly`` () =
    use file = new TempFile()
    use db = new LiteRepository(file.FileName)
    prepareDatabase db
    let settings = Mock<ISettings>().Setup(fun s -> <@ s.GetServerAddress() @>).Returns(Task.FromResult("server"))
                                    .Setup(fun s -> <@ s.GetUserName() @>).Returns(Task.FromResult("user"))
                                    .Setup(fun s -> <@ s.GetPassword() @>).Returns(Task.FromResult("password")).Create()
    let service = new NoteService(file.FileName, settings, connectivity)
    let response = new PropfindResponse(200, new System.Collections.Generic.List<WebDavResource>())
    let resourceBuilder = new WebDavResource.Builder()
    (response.Resources :?> System.Collections.Generic.List<WebDavResource>).Add(resourceBuilder
                                                                                  .IsCollection()
                                                                                  .WithUri("/CloudNotes/").Build())
    (response.Resources :?> System.Collections.Generic.List<WebDavResource>).Add(resourceBuilder
                                                                                  .IsNotCollection()
                                                                                  .WithUri(sprintf "/CloudNotes/%s" serverNoteId).Build())
    let serverNote = new Note(Id = serverNoteId, Title = "Note2", Content = "Note2", LastChanged = lastChangedDate)
    let json = JsonConvert.SerializeObject(serverNote)
    use stream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(json))
    let webDavClient = Mock<IWebDavClient>()
                        .Setup(fun c -> <@ c.Propfind(It.IsAny<string>()) @>)
                        .Returns(Task.FromResult(response))
                        .Setup(fun c -> <@ c.PutFile(It.IsAny<string>(), It.IsAny<Stream>()) @>)
                        .Returns(Task.FromResult(new WebDavResponse(200)))
                        .Setup(fun c -> <@ c.Delete(It.IsAny<string>()) @>)
                        .Returns(Task.FromResult(new WebDavResponse(200)))
                        .Setup(fun c -> <@ c.GetRawFile(It.IsAny<string>()) @>)
                        .Returns(Task.FromResult(new WebDavStreamResponse(new HttpResponseMessage(), stream)))
                        .Create()
    let webDavClientFactory = Mock<IWebDavClientFactory>()
                               .Setup(fun f -> <@ f.CreateWebDavClient(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()) @>)
                               .Returns(webDavClient).Create()

    let result = service.SyncNotes(webDavClientFactory).Result
    let recordsCount = db.Query<Note>().Count()

    result.Succeed |> should be True
    recordsCount |> should equal 2
    Mock.Verify(<@ webDavClient.PutFile(It.IsAny<string>(), It.IsAny<Stream>()) @> , once)
    Mock.Verify(<@ webDavClient.Delete(It.IsAny<string>()) @>, never)
    Mock.Verify(<@ webDavClient.GetRawFile(It.IsAny<string>()) @>, once)