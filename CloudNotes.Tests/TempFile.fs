﻿module CloudNotes.Tests

open System
open System.IO

type TempFile() =
    let mutable fileName = Path.Combine(Path.GetTempPath(), (sprintf "test-%s.db" (Guid.NewGuid().ToString())))

    member this.FileName
        with get() = fileName

    interface IDisposable with
        member this.Dispose() =
            File.Delete this.FileName